import React,{Component} from 'react';


class App extends Component

{
  constructor()
  {
    super();
    this.state ={
      count: 0
    }
  }

  addValue = () =>
  {
    this.setState({
      count: this.state.count + 1
    })
  }
  decValue= () =>
  
  {
    this.setState({
      count: this.state.count - 1
    })
  }

render()
{
  return(
    <div>
      <h1>Counter Demo</h1>   
      <h3>Counter: {this.state.count}</h3>
      <button onClick={this.addValue}>Increment counter</button>
      <button onClick={this.decValue}>Decrement counter</button>
    </div>
)
}
}

export default App;
